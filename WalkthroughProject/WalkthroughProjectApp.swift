//
//  WalkthroughProjectApp.swift
//  WalkthroughProject
//
//  Created by shreejwal giri on 16/05/2021.
//

import SwiftUI

@main
struct WalkthroughProjectApp: App {
    var body: some Scene {
        WindowGroup {
            HomeView()
        }
    }
}
