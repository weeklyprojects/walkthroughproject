//
//  HomeView.swift
//  WalkthroughProject
//
//  Created by shreejwal giri on 20/05/2021.
//

import SwiftUI

struct HomeView: View {
    
    @State var index: Int = 1
    
    var contentDataArr = [
        ContentData(title: "Get Inspired", description: "A paragraph is a self-contained unit of discourse in writing dealing with a particular point or idea. A paragraph consists of one or more sentences. ", icon: "ic-one", backgroundColor: Color(#colorLiteral(red: 0.7647058824, green: 0.9019607843, blue: 0.8588235294, alpha: 1))),
        ContentData(title: "this is the title", description: "Though not required by the syntax of any language, paragraphs are usually an expected part of formal writing, used to organize longer prose", icon: "ic-two", backgroundColor: Color(#colorLiteral(red: 0.9921568627, green: 0.8196078431, blue: 0.4823529412, alpha: 1))),
        ContentData(title: "this is the title", description: "Though not required by the syntax of any language, paragraphs are usually an expected part ", icon: "ic-three", backgroundColor: Color(#colorLiteral(red: 0.6588235294, green: 0.7333333333, blue: 0.8705882353, alpha: 1)))
    ]
    
    var body: some View {
        
        GeometryReader { (geo) in
            ZStack {
                if index <= contentDataArr.count {
                    if index == 1 {
                        ChildView(contentData: contentDataArr[0], backgroundColor: contentDataArr[0].backgroundColor, geo: geo.size, currentIndex: $index)
                    }
                    
                    if index == 2 {
                        ChildView(contentData: contentDataArr[1], backgroundColor: contentDataArr[1].backgroundColor, geo: geo.size, currentIndex: $index)
                    }
                    
                    if index == 3 {
                        ChildView(contentData: contentDataArr[2], backgroundColor: contentDataArr[2].backgroundColor, geo: geo.size, currentIndex: $index)
                    }
                } else {
                    ChildView(contentData: contentDataArr[2], backgroundColor: contentDataArr[2].backgroundColor, geo: geo.size, currentIndex: $index)
                }
                
            }
            .overlay(
                Button(action: {
                    withAnimation {
                        index += 1
                    }
                }, label: {
                    Image(systemName: "chevron.right")
                        .font(.system(size: 25, weight: .semibold, design: .rounded))
                        .foregroundColor(.black)
                        .frame(width: 50, height: 50)
                        .background(Color.white)
                        .clipShape(Circle())
                        .overlay(
                            
                            ZStack {
                                Circle()
                                    .stroke(Color.black.opacity(0.04), lineWidth: 4)
                                
                                Circle()
                                    .trim(from: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/, to: CGFloat(index) / CGFloat(contentDataArr.count))
                                    .stroke(Color.white, lineWidth: 4)
                            }
                            .padding(-15)
                            
                        )
                })
                .padding(.bottom, 50)
                ,alignment: .bottom
            )
            
            
        }
        
        
    }
    
}

struct BlankView: View {
    var body: some View {
        ZStack {
           Text("Hello, Shreejwal  Giri")
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
