//
//  ContentView.swift
//  WalkthroughProject
//
//  Created by shreejwal giri on 16/05/2021.
//

import SwiftUI

struct ContentData: Identifiable {
    let id = UUID()
    let title: String
    let description: String
    let icon: String
    let backgroundColor: Color
}

struct ContentView: View {
    
    var contentDataArr = [
        ContentData(title: "Get Inspired", description: "A paragraph is a self-contained unit of discourse in writing dealing with a particular point or idea. A paragraph consists of one or more sentences. ", icon: "ic-one", backgroundColor: Color(#colorLiteral(red: 0.7647058824, green: 0.9019607843, blue: 0.8588235294, alpha: 1))),
        ContentData(title: "this is the title", description: "Though not required by the syntax of any language, paragraphs are usually an expected part of formal writing, used to organize longer prose", icon: "ic-two", backgroundColor: Color(#colorLiteral(red: 0.9921568627, green: 0.8196078431, blue: 0.4823529412, alpha: 1))),
        ContentData(title: "this is the title", description: "Though not required by the syntax of any language, paragraphs are usually an expected part ", icon: "ic-three", backgroundColor: Color(#colorLiteral(red: 0.6588235294, green: 0.7333333333, blue: 0.8705882353, alpha: 1)))
    ]
    
    var body: some View {
        ScrollView {
            TabView {
                ForEach(contentDataArr) { data in
                    ChildView(contentData: data, backgroundColor: data.backgroundColor, geo: UIScreen.main.bounds.size, currentIndex: Binding.constant(0))
                }
            }
            .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            .tabViewStyle(PageTabViewStyle(indexDisplayMode: .never))
        }
        .edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
    }
}

struct ChildView: View {
    @State var contentData: ContentData
    @State var backgroundColor: Color = .white
    @State var geo: CGSize = CGSize(width: 0, height: 0)
    @Binding var currentIndex: Int
    var body: some View {
        ZStack {
            VStack {
                Spacer()
                
                Image(contentData.icon)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(height: geo.height / 2)
                VStack(alignment: .center, spacing: 16, content: {
                    Text(contentData.title)
                        .font(.system(size: 25))
                        .bold()
                    Text(contentData.description)
                        .multilineTextAlignment(.center)
                        .foregroundColor(.black)
                        .font(.system(size: 18))
                })
                .padding([.leading, .trailing], 16)
                
                Spacer()
                Spacer()
                
//                HStack {
//                    Button(action: {
//                        
//                    }, label: {
//                        Text("Skip")
//                    })
//                    
//                    Spacer()
//                    
//                    Button(action: {
//                        currentIndex += 1
//                    }, label: {
//                        HStack {
//                            Text("Next")
//                                .fontWeight(.medium)
//                            Image(systemName: "arrow.right")
//                                .resizable()
//                                .frame(width: 20, height: 20, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
//                        }
//                        
//                    })
//                }
                .padding([.leading, .trailing], 16)
                .foregroundColor(.black)
                
                Spacer()
            }
        }
        .background(backgroundColor).edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
